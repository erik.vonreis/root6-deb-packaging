#!/bin/sh
apt-get install ca-certificates

cat <<EOF > /etc/apt/apt.conf.d/80unauth
APT::Get::AllowUnauthenticated "true";
EOF

cat <<EOF > /etc/apt/sources.list.d/lscsoft_root.list
deb [trusted=yes] https://hypatia.aei.mpg.de/lsc-amd64-buster/ /
EOF
