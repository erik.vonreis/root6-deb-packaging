rootver = 6.24.00
rootv = 6.24.0
cdsver = $(rootv)deb10u1
dscfile = build/root-cds_$(cdsver).dsc
debian-files = changelog control rules source compat copyright triggers
distro = buster
basepath = /var/cache/pbuilder/base-$(distro).cow

src: $(dscfile)

$(dscfile): build/root6 build/root6/debian
	cd build && dpkg-source -b root6

build/root_v$(rootver).source.tar.gz:
	mkdir -p build
	cd build && wget "https://root.cern/download/root_v$(rootver).source.tar.gz"

build/root6: build/root_v$(rootver).source.tar.gz
	cd build && tar xzf root_v$(rootver).source.tar.gz
	mv "build/root-$(rootver)" build/root6

build/root6/debian: build/root6 $(debian-files)
	mkdir -p build/root6/debian
	cp -r $(debian-files) build/root6/debian
	touch build/root6/debian

package: $(dscfile)
	sudo cowbuilder build --distribution $(distro) --use-network yes $(dscfile) --basepath $(basepath)

update:
	sudo cowbuilder update --distribution $(distro)  --basepath $(basepath)
	
create:
	mkdir -p .cowbuilder
	sudo cowbuilder create --distribution $(distro) --basepath $(basepath)

clean:
	rm -rf build
